import React, { Component } from "react";

export default class Item extends Component {
  render() {
    return (
      <div>
        <div class="row">
          <div class="mb-5 col-sm-12 col-md-6 col-xl-3">
            <div class="card">
              <div class="card-body text-center bg bg-secondary py-5">
                500 x 325
              </div>

              <div class="card-body text-center">
                <h5 class="card-title">Card Title</h5>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Ipsa, natus?
                </p>
              </div>

              <div class="card-body text-center bg bg-light">
                <button class="btn btn-primary">Find Out More!</button>
              </div>
            </div>
          </div>

          <div class="mb-5 col-sm-12 col-md-6 col-xl-3">
            <div class="card">
              <div class="card-body text-center bg bg-secondary py-5">
                500 x 325
              </div>

              <div class="card-body text-center">
                <h5 class="card-title">Card Title</h5>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Ipsa, natus?
                </p>
              </div>

              <div class="card-body text-center bg bg-light">
                <button class="btn btn-primary">Find Out More!</button>
              </div>
            </div>
          </div>
          <div class="mb-5 col-sm-12 col-md-6 col-xl-3">
            <div class="card">
              <div class="card-body text-center bg bg-secondary py-5">
                500 x 325
              </div>

              <div class="card-body text-center">
                <h5 class="card-title">Card Title</h5>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Ipsa, natus?
                </p>
              </div>

              <div class="card-body text-center bg bg-light">
                <button class="btn btn-primary">Find Out More!</button>
              </div>
            </div>
          </div>
          <div class="mb-5 col-sm-12 col-md-6 col-xl-3">
            <div class="card">
              <div class="card-body text-center bg bg-secondary py-5">
                500 x 325
              </div>

              <div class="card-body text-center">
                <h5 class="card-title">Card Title</h5>
                <p class="card-text">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  Ipsa, natus?
                </p>
              </div>

              <div class="card-body text-center bg bg-light">
                <button class="btn btn-primary">Find Out More!</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
