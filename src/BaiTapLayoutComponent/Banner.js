import React, { Component } from "react";

export default class Banner extends Component {
  render() {
    return (
      <div>
        <div class="carousel_title py-4">
          <h3>A Warm Welcome!</h3>

          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Atque,
            totam sapiente! Fuga natus enim ipsa quasi culpa facilis in aut!
          </p>
          <button class="btn btn-primary">Call to Action!</button>
        </div>
      </div>
    );
  }
}
