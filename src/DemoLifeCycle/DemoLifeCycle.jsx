import React, { Component } from "react";
import Child from "./Child";
import axios from "axios";

export default class DemoLifeCycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      message: "Hello",
      number: 1,
      isLiked: false,
      listProduct: [],
    };
    console.log("constructor");
    // constructor chay truoc de khai bao state va cac gia tri khac
  }

  // method componentWillMount was used for version before 16.4

  // method getDerivedStateFromProps run before render, it is required return: null or key={value}
  //
  static getDerivedStateFromProps(props, state) {
    console.log("getDerivedStateFromProps", props, state);
    return {
      ...state,
      message: "Goodbye",
    };
  }

  // shouldComponentUpdate run after getDerived... and before render
  shouldComponentUpdate() {
    return true;
  }

  getSnapshotBeforeUpdate() {
    console.log("getSnapshotBeforeUpdate");
    return { message: "snapshot" };
  }

  componentDidUpdate() {
    let getParamUrl = window.location.search;
    let searchParam = new URLSearchParams(getParamUrl);
    let id = searchParam.get("id");
    if (id) {
      //call api detail
    }
    console.log(searchParam.get("id"));
    // console.log("componentDidUpdate");
    // this.setState({ number: this.state.number });
  }

  getProductListApi() {
    axios
      .get("https://shop.cyberlearn.vn/api/Product")
      .then((result) => {
        console.log("data", result);
        this.setState({ listProduct: result.data.content });
      })
      .catch((error) => console.log("error"));
  }
  render() {
    console.log("render", this.state.listProduct);
    const { number } = this.state;

    return (
      <div className="container">
        DemoLifeCycle
        <h3>List of Products</h3>
        <div className="row">
          {this.state.listProduct.map((item, index) => {
            return (
              <div className="col-4" key={index}>
                <div className="card text-left">
                  <img
                    src={item.image}
                    className="card-img-top"
                    style={{ width: "20%" }}
                    alt
                  />
                  <div className="card-body">
                    <h4 className="card-title">{item.alias}</h4>
                    <p className="card-text">Body</p>
                  </div>
                  <div>
                    <button className="btn btn-primary">View Details</button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
        {/* <p>{number}</p>
        <button
          className="btn btn-success"
          onClick={() => {
            this.setState({ number: number + 1 });
          }}
        >
          Increase number
        </button>
        <Child number={number} /> */}
      </div>
    );
  }

  componentDidMount() {
    // call api => data
    console.log("componentDidMount");
    this.getProductListApi();
  }
}
