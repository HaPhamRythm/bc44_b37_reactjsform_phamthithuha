import React, { Component } from "react";
import axios from "axios";
export default class Re_DemoLifeCycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listProduct: [],
    };
  }
  getProductListApi() {
    axios
      .get("https://shop.cyberlearn.vn/api/Product")
      .then((result) => {
        // console.log(result);
        this.setState({ listProduct: result.data.content });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render() {
    console.log("render", this.state.listProduct);
    return (
      // https://shop.cyberlearn.vn/swagger/index.html?fbclid=IwAR1pGAuhtafxCeHJvy3cL1521nOGdRhQ0IDCG5o51RwmRIaAeRYt3uC1_ok
      <div className="container">
        <h1>List of Product</h1>
        <div className="row">
          {this.state.listProduct.map((item) => {
            return (
              <div className="col-4 mb-3" key={item.id}>
                <div className="card text-left h-100">
                  <img className="card-img-top" src={item.image} alt />
                  <div className="card-body">
                    <h4 className="card-title">{item.alias}</h4>
                    <button className="btn btn-success">View Details</button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  componentDidMount() {
    this.getProductListApi();
  }
}
