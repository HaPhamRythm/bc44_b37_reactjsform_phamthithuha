import { studentsReducer } from "./studentsReducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({ studentsReducer });
