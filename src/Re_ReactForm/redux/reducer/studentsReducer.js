import {
  ADD_NEW_STUDENT,
  DELETE,
  EDIT1,
  ONCHANGE,
} from "../constant/constantStudents";

let initialState = {
  studentArray: [],
  editedStudent: null,
};
export const studentsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_NEW_STUDENT: {
      const cloneArray = [...state.studentArray];
      cloneArray.push(payload);
      state.studentArray = cloneArray;
      return { ...state };
    }
    case DELETE: {
      const newArray = state.studentArray.filter((item) => item.id !== payload);
      return { ...state, studentArray: newArray };
    }

    case EDIT1: {
      const findStudent = state.studentArray.find((item) => {
        return item.id === payload.id;
      });
      state.editedStudent = findStudent;
      return { ...state };
    }
    default:
      return state;
  }
};
