import React, { Component } from "react";

export default class ItemGlass extends Component {
  render() {
    return (
      <div className="col-4 py-2 bg-white" style={{ cursor: "pointer" }}>
        <div
          className="text-center h-100"
          onClick={() => {
            this.props.handleChangeToItem(this.props.data);
          }}
        >
          <img
            style={{ width: 100 }}
            className="card-img-top "
            src={this.props.data.image}
            alt
          />
        </div>
      </div>
    );
  }
}
