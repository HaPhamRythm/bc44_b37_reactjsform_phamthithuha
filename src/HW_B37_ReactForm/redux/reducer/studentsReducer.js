import {
  ADD_NEW_STUDENT,
  DELETE,
  EDIT1,
  ONCHANGE,
} from "../constant/constantStudents";

let initialState = {
  studentArray: [],
  editedStudent: {},
};
export const studentsReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_NEW_STUDENT: {
      const cloneArray = [...state.studentArray];
      const index = cloneArray.findIndex((item) => item.id === payload.id);
      if (index !== -1) {
        alert("Id is existing");
      } else {
        cloneArray.push(payload);
      }
      return { ...state, studentArray: cloneArray };
    }
    case DELETE: {
      const newArray = state.studentArray.filter((item) => item.id !== payload);
      return { ...state, studentArray: newArray };
    }

    case EDIT1: {
      document.getElementById("id").disabled = true;

      return { ...state, editedStudent: payload };
    }
    default:
      return state;
  }
};
