import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./data";
import CartShoe from "./CartShoe";

export default class Re_ShoeShop_Redux extends Component {
  state = {
    shoeArr: shoeArr,
    detail: shoeArr[0],
    cart: [],
  };

  handleClick = (selectedShoe) => {
    this.setState({
      detail: selectedShoe,
    });
  };

  handleBuy = (wantedShoe) => {
    let cloneCart = [...this.state.cart];

    let index = cloneCart.findIndex((item) => item.id == wantedShoe.id);
    if (index == -1) {
      let newShoe = { ...wantedShoe, number: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].number++;
    }

    // cloneCart.push(wantedShoe);
    this.setState({
      cart: cloneCart,
    });
  };

  handleDelete = (idShoe) => {
    let cloneCart = this.state.cart.filter((item) => item.id !== idShoe);
    this.setState({
      cart: cloneCart,
    });
  };

  handleNumber = (idShoe, option) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == idShoe);
    if (cloneCart[index].number == 0) {
      cloneCart.splice(index, 1);
    } else {
      cloneCart[index].number += option;
    }
    this.setState({
      cart: cloneCart,
    });
  };
  render() {
    return (
      <div>
        <h1>ShoeShop</h1>
        <div className="row">
          <CartShoe
          // cart={this.state.cart}
          // handleDeleteToCart={this.handleDelete}
          // handleNumberToCart={this.handleNumber}
          />
          <ListShoe
          // listToRender={this.state.shoeArr}
          // handleClickToListShoe={this.handleClick}
          // handleBuyToListShoe={this.handleBuy}
          />
        </div>

        <DetailShoe
        // detailOfSelectedShoe={this.state.detail}
        />
      </div>
    );
  }
}
