import React, { Component } from "react";
import ListShoe from "./ListShoe";
import DetailShoe from "./DetailShoe";
import { shoeArr } from "./data";
import CartShoe from "./CartShoe";

export default class Ex_ShoeShop extends Component {
  state = {
    shoeArr: shoeArr,
    detail: shoeArr[0],
    cart: [],
  };
  handleDetail = (shoe) => {
    this.setState({
      detail: shoe,
    });
  };

  handleBuy = (shoe) => {
    let cloneCart = [...this.state.cart];
    let index = cloneCart.findIndex((item) => item.id == shoe.id);
    if (index == -1) {
      let newShoe = { ...shoe, number: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].number++;
    }
    this.setState({
      cart: cloneCart,
    });
  };

  handleDelete = (idshoe) => {
    let keptShoes = this.state.cart.filter((item) => item.id !== idshoe);
    this.setState({
      cart: keptShoes,
    });
  };
  render() {
    return (
      <div>
        <h2>ShoeShop</h2>
        <CartShoe cart={this.state.cart} handleDelete={this.handleDelete} />
        <ListShoe
          arrList={this.state.shoeArr}
          handleDetailList={this.handleDetail}
          handleBuyList={this.handleBuy}
        />
        <DetailShoe detail={this.state.detail} />
      </div>
    );
  }
}
