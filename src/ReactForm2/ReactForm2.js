import React, { Component } from "react";
import ProductList from "./ProductList";

export default class ReactForm2 extends Component {
  state = {
    product: {
      id: "",
      image: "",
      name: "",
      price: "",
    },
    productArray: [],
    error: {
      id: "",
      image: "",
      name: "",
      price: "",
    },
    editingProduct: null,
  };

  handleOnchange = (event) => {
    const { id, value, pattern } = event.target;
    const newProduct = { ...this.state.product, [id]: value };
    const newError = { ...this.state.error };
    if (!value.trim()) {
      newError[id] = "Please fill the blank";
    } else {
      if (pattern == "^[0-9]{4,6}$") {
        let reg = /^[0-9]{4,6}$/;
        if (!reg.test(value)) {
          newError[id] = "Please enter 4-6 integers";
        } else {
          newError[id] = "";
        }
      }

      if (pattern == "^[a-zA-Z ]+$") {
        let nameValidation = /^[a-zA-Z ]+$/;
        if (!nameValidation.test(value)) {
          newError[id] = "Please enter alphabets ";
        } else {
          newError[id] = "";
        }
      }
      //not accept decimals, negative number and numbers with leading zero
      if (pattern == "^[1-9]d{0,5}(?:.d{1,2})?$") {
        let priceValidation = /^[1-9]d{0,5}(?:.d{1,2})?$/;
        if (!priceValidation.test(value)) {
          newError[id] = "Please enter integers";
        } else {
          newError[id] = "";
        }
      }
    }
    this.setState({ product: newProduct, error: newError });
    console.log(newProduct);
  };

  handleAdd = (event) => {
    event.preventDefault();
    let valid = true;
    Object.values(this.state.error).forEach((item) => {
      if (item.length > 0) {
        valid = false;
      }
      if (valid) {
        const newProductArray = [
          ...this.state.productArray,
          this.state.product,
        ];
        this.setState({ productArray: newProductArray });
      }
    });
  };

  handleDelete = (product) => {
    const cloneProductArray = [...this.state.productArray];
    const index = cloneProductArray.findIndex((item) => {
      return item.id === product.id;
    });
    cloneProductArray.splice(index, 1);
    this.setState({ productArray: cloneProductArray });
  };

  handleEdit1 = (idProduct) => {
    const findProduct = this.state.productArray.find((item) => {
      return item.id === idProduct;
    });
    this.setState({ editingProduct: findProduct, product: findProduct });
  };

  handleEdit2 = () => {
    const index = this.state.productArray.findIndex((item) => {
      return item.id === this.state.editingProduct.id;
    });
    const newArray = [...this.state.productArray];
    newArray[index] = this.state.product;
    this.setState({
      productArray: newArray,
      product: {
        id: "",
        image: "",
        name: "",
        price: "",
      },

      editingProduct: null,
    });
  };
  render() {
    let { id, name, image, price } = this.state.product;
    let { error } = this.state;
    return (
      <div className="container text-left">
        <h3>Product Info</h3>
        <form onSubmit={this.handleAdd}>
          <div className="row">
            <div className="col-6">
              <label htmlFor="">Id</label>
              <input
                value={id}
                type="text"
                name="id"
                id="id"
                className="form-control"
                onChange={this.handleOnchange}
                pattern="^[0-9]{4,6}$"
              />
              <span className="text text-danger">{error.id}</span>
            </div>
            <div className="col-6">
              <label htmlFor="">Image</label>

              <input
                value={image}
                type="text"
                name="image"
                id="image"
                className="form-control"
                onChange={this.handleOnchange}
              />
            </div>
            <div className="col-6">
              <label htmlFor="">Name</label>
              <input
                value={name}
                type="text"
                name="name"
                id="name"
                className="form-control"
                onChange={this.handleOnchange}
                pattern="^[a-zA-Z ]+$"
              />
              <span className="text text-danger">{error.name}</span>
            </div>
            <div className="col-6">
              <label htmlFor="">Price</label>
              <input
                value={price}
                type="text"
                name="price"
                id="price"
                className="form-control"
                onChange={this.handleOnchange}
                pattern="^[1-9]d{0,5}(?:.d{1,2})?$"
              />
              <span className="text text-danger">{error.price}</span>
            </div>
            <button className="btn btn-success">Add</button>
            <button
              onClick={() => {
                this.handleEdit2();
              }}
              className="btn btn-primary"
            >
              Edit
            </button>
          </div>
        </form>

        <ProductList
          productArray={this.state.productArray}
          handleDelete={this.handleDelete}
          handleEdit1={this.handleEdit1}
        />
      </div>
    );
  }
}
