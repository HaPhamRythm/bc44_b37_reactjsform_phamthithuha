import React, { Component } from "react";
import moment from "moment";
export default class Item extends Component {
  convertName = (title) => {
    if (title.length > 10) {
      return title.slice(0, 10) + "...";
    }
    return title;
  };
  render() {
    let { handleDetailItem, movie } = this.props;
    let { tenPhim, hinhAnh, ngayKhoiChieu } = movie;
    return (
      <div className="col-3">
        <div className="card text-left h-100 mb-2">
          <img
            style={{ height: 200 }}
            className="card-img-top"
            src={hinhAnh}
            alt
          />
          <div className="card-body">
            <h4 className="card-title">{this.convertName(tenPhim)}</h4>
            <p className="card-text">
              {moment(ngayKhoiChieu).format("DD/MM/YYYY")}
            </p>
          </div>
          <button
            onClick={() => {
              handleDetailItem(movie);
            }}
            className="btn btn-danger"
          >
            Detail
          </button>
        </div>
      </div>
    );
  }
}
